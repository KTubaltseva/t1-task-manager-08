package ru.t1.ktubaltseva.tm;

import ru.t1.ktubaltseva.tm.api.ICommandRepository;
import ru.t1.ktubaltseva.tm.constant.ArgumentConst;
import ru.t1.ktubaltseva.tm.constant.CommandConst;
import ru.t1.ktubaltseva.tm.model.Command;
import ru.t1.ktubaltseva.tm.repository.CommandRepository;
import ru.t1.ktubaltseva.tm.util.FormatUtil;

import java.util.Scanner;

public class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(String[] args) {
        processArgument(args);
        processCommands();
    }

    private static void processArgument(final String[] args) {
        if (args == null || args.length < 1) return;
        final String argument = args[0];
        switch (argument) {
            case ArgumentConst.VERSION:
                displayVersion();
                break;
            case ArgumentConst.ABOUT:
                displayAbout();
                break;
            case ArgumentConst.HELP:
                displayHelp();
                break;
            case ArgumentConst.INFO:
                displaySystemInfo();
                break;
            default:
                displayArgumentError();
        }
        System.exit(0);
    }

    private static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.VERSION:
                displayVersion();
                break;
            case CommandConst.ABOUT:
                displayAbout();
                break;
            case CommandConst.HELP:
                displayHelp();
                break;
            case CommandConst.INFO:
                displaySystemInfo();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                displayCommandError();
        }
    }

    private static void processCommands() {
        System.out.println("** WELCOME TO TASK MANAGER **");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final Scanner scanner = new Scanner(System.in);
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.8.0");
    }

    private static void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Tubaltseva Ksenia");
        System.out.println("email: ktubaltseva@t1-consulting.ru");
    }

    private static void displayHelp() {
        System.out.println("[HELP]");
        final Command[] commands = COMMAND_REPOSITORY.getCommands();
        for (final Command command: commands) System.out.println(command);
        System.out.println();
    }

    private static void displayArgumentError() {
        System.err.println("Invalid argument");
        System.exit(1);
    }

    private static void displayCommandError() {
        System.err.println("Invalid command");
        displayHelp();
    }

    private static void displaySystemInfo() {
        final Runtime runtime = Runtime.getRuntime();

        final int availableProcessors = runtime.availableProcessors();
        final long freeMemory = runtime.freeMemory();
        final long maxMemory = runtime.maxMemory();
        final long totalMemory = runtime.totalMemory();
        final long usageMemory = totalMemory - freeMemory;

        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;

        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        final String maxMemoryFormat = (maxMemoryCheck ? "no limit" :  FormatUtil.formatBytes(maxMemory));
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);

        System.out.printf("Available processors (cores):\t %s\n", availableProcessors);
        System.out.println();
        System.out.printf("Free memory:\t %s\n", freeMemoryFormat);
        System.out.printf("Maximum memory:\t %s\n", maxMemoryFormat);
        System.out.printf("Total memory:\t %s\n", totalMemoryFormat);
        System.out.printf("Usage memory:\t %s\n", usageMemoryFormat);
        System.out.println();
    }

    private static void exit() {
        System.out.println("[EXIT]");
        System.exit(0);
    }

}